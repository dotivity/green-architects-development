var controller = new ScrollMagic.Controller();

function pathPrepare($el) {
  var lineLength = $el[0].getTotalLength();
  $el.css("stroke-dasharray", lineLength);
  $el.css("stroke-dashoffset", lineLength);
}

var $route = $("rect#route-1");

// prepare SVG
pathPrepare($route);

// build tween
var route_1 = new TimelineMax()
  .add(TweenMax.to($route, 0.9, {
    strokeDashoffset: 0,
    ease: Linear.easeNone
  }));

var scene = new ScrollMagic.Scene({
    triggerElement: "#trigger-route-1",
    duration: "50%",
    triggerHook: 0.75,
    tweenChanges: true
  })
  .setTween(route_1)
  //.addIndicators()
  .addTo(controller);
