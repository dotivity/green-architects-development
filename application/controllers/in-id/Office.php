<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('gallery_model');
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->library('pagination');


  }

  // fungsi untuk mengambil data
  public function index()
  {

    $data['data'] = $this->gallery_model->get();
    $this->data['title']   = 'Office - Green Architects';
    $this->load->view('frontend/id/visit_office/index',$this->data);

  }
}
