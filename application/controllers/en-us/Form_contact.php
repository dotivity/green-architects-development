<?php defined('BASEPATH') OR exit('No direct script access allowed');

ob_start();

//Load composer's autoloader
require 'vendor/autoload.php';

class Form_contact extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->library('upload');
		$this->load->library('email');
	}

	function formEmail(){
		$name       = $this->input->post('name'); // Menangkap inputan nama dari form
		$email      = $this->input->post('email'); // Menangkap inputan email dari form
		$recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
		$attachment = $this->input->post['resume'];

		$this->postEmail($name,$email,$recaptchaResponse);
	}

	public function postEmail(){
		$name       = $this->input->post('name'); // Menangkap inputan nama dari form
		$email      = $this->input->post('email'); // Menangkap inputan email dari form
		// Load PHPMailer library
		$this->load->library('phpmailer_lib');
		// PHPMailer object
		$mail = $this->phpmailer_lib->load();
		// SMTP configuration
		$mail->isSMTP();
		$mail->SMTPDebug = 2;
		$mail->Host     = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'infobygreenarchitects@gmail.com'; //marketing@imx.co.id
		$mail->Password = 'greenarchitects2020!'; //marketingimx2020#@!
		$mail->SMTPSecure = 'tls';
		$mail->Port     = 587;
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		$mail->setFrom('careers@bygreenarchitects.com', 'Website Careers bygreenarchitects.com'); //marketing@imx.co.id
		$mail->addReplyTo('info@bygreenarchitects.com', 'bygreenarchitects.com'); //marketing@imx.co.id
		// Add a recipient
		$mail->addAddress('info@bygreenarchitects.com');
		// Email subject
		$mail->Subject = 'Email from Website bygreenarchitects.com';
		// Attachments
		$mail->AddAttachment($_FILES['resume']['tmp_name'], $_FILES['resume']['name']);		// Set email format to HTML
		$mail->isHTML(true);
		// Email body content
		$mailContent = "<h1>Email from Website bygreenarchitects.com</h1>
		<p><b>Name :</b> $name, <b>Email :</b> $email</p>";
		$mail->Body = $mailContent;

		$userIp=$this->input->ip_address();

		$secret= $this->config->item('google_secret');

		$credential = array(
			'secret' => $secret,
			'response' => $this->input->post('g-recaptcha-response')
		);

		$verify = curl_init();
		curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
			curl_setopt($verify, CURLOPT_POST, true);
			curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
			curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($verify);

			$status= json_decode($response, true);



			if($status['success']){
				$mail->send();
				$this->session->set_flashdata('message', 'Google Recaptcha Successful');
				//redirect(base_url('en-us/mail_sent'));
			}else{
				$this->session->set_flashdata('message', 'Sorry Google Recaptcha Unsuccessful!!');
			//redirect(base_url('en-us/mail_failed'));
			}

		}
	}
