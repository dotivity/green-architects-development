<section class="our-news section-margin">
    <div class="container">
        <div class="one-title" data-dsn-animate="up">
            <div class="title-sub-container">
                <p class="title-sub">Latest News</p>
            </div>
            <h2 class="title-main">Article</h2>
        </div>
        <div class="custom-container">
            <div class="slick-slider">
                <div class="item-new slick-slide">
                    <div class="image" data-overlay="5">
                        <img src="assets/img/blog/1.jpg" alt="">
                    </div>
                    <div class="content">
                        <div class="background"></div>
                        <h5>Web , Brand</h5>

                        <div class="cta">
                            <a href="#">Digital Photography Tips</a>
                        </div>

                        <p>Simple point-and-shoot digital cameras can give surprising quality when they
                            have the right lenses and sensors.</p>

                    </div>
                </div>

                <div class="item-new slick-slide">
                    <div class="image" data-overlay="5">
                        <img src="assets/img/blog/2.jpg" alt="">
                    </div>
                    <div class="content">
                        <div class="background"></div>
                        <h5>Web , Brand</h5>

                        <div class="cta">
                            <a href="#">Digital Photography Tips</a>
                        </div>

                        <p>Simple point-and-shoot digital cameras can give surprising quality when they
                            have the right lenses and sensors.</p>
                    </div>
                </div>

                <div class="item-new slick-slide">
                    <div class="image" data-overlay="5">
                        <img src="assets/img/blog/3.jpg" alt="">
                    </div>
                    <div class="content">
                        <div class="background"></div>
                        <h5>Web , Brand</h5>

                        <div class="cta">
                            <a href="#">Digital Photography Tips</a>
                        </div>

                        <p>Simple point-and-shoot digital cameras can give surprising quality when they
                            have the right lenses and sensors.</p>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
