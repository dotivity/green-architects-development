<section class="client-see-v2 section-margin">
    <div class="container">
        <div class="inner">
            <div class="left">
                <h2 class="title" data-dsn-grid="move-section" data-dsn-move="-60"
                    data-dsn-duration="100%" data-dsn-opacity="1" data-dsn-responsive="tablet">
                    <span class="text">Our Mission</span>
                </h2>
            </div>

            <div class="items">
                <div class="bg"></div>
                <div class="slick-slider">
                    <div class="item">
                        <div class="quote">
                            <p><b>INDONESIA’S ARCHITECTURE OF THE 21ST CENTURY</b> <br> <br>Creating buildings across Indonesia to be equally forward-looking and provides a good quality of light, sanitation, and well-being.</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="quote">
                            <p><b>GREEN INTENSIVE</b> <br> <br>Act to provide more access to green spaces, especially in urban areas.</p>
                        </div>

                    </div>

                    <div class="item">
                        <div class="quote">
                            <p><b>A BUILDING THAT INSPIRES AND EDUCATES</b> <br> <br>Making architectural bodies that stand out in the world. Which will increase educational and tourism value. A collective good-quality architecture will call the world for attention.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
