<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-US">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="title" content="Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS" />
  <meta name="description" content="GREEN ARCHITECTS adalah perusahaan Konsultan arsitektur yang secara khusus berfokus pada bidang Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design.">
  <meta name="keywords" content=" GREEN ARCHITECTS - Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS ">
  <meta name="author" content=" GREEN ARCHITECTS ">
  <meta name="identifier-URL" content="https://www.bygreenarchitects.com">
  <meta property="og:title" content="Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS"/>
  <meta property="og:url" content="https:// www.bygreenarchitects.com "/>
  <meta property="og:type" content="website" />
  <meta property="og:description" content="GREEN ARCHITECTS adalah perusahaan Konsultan arsitektur yang secara khusus berfokus pada bidang Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design."/>
  <meta property="og:image" content="<?php echo base_url ('assets_frontend/meta/meta-home.jpg')?>">
  <meta property="og:site_name" content=" Jasa Arsitek Jakarta – GREEN ARCHITECTS"/>

  <!--  Title -->
  <title>By Green Architects</title>

  <!-- Font Google -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700&amp;display=swap" rel="stylesheet">

  <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend/img/ico/favicon.ico')?>" type="image/x-icon" />
  <link rel="icon" href="<?php echo base_url ('assets_frontend/img/ico/favicon.ico')?>" type="image/x-icon" />

  <!-- custom styles (optional) -->
  <link href="<?php echo base_url ('assets_frontend/css/plugins.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url ('assets_frontend/css/style.css')?>" rel="stylesheet" />
</head>
