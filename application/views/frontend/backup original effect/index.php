<?php
include "html.php";
include "openbody.php";
include "preloader.php";
include "nav.php";
include "header.php";
include "about.php";
include "vision.php";
include "mission.php";
include "corporatevalues.php";
include "greetings.php";
include "portfolio.php";
include "ourapproach.php";
include "ourspecialisation.php";
//include "article.php";
include "partnership.php";
include "whatwedo.php";
include "maps.php";
include "footer.php";
include "waitloader.php";
include "cursor.php";
include "js.php";
include "closebody.php";
include "end.php";
 ?>
<!-- Structure
1. html
2. openbody
3. preloader
4. nav
5. header
6. about
7. corporatevalues
8. greetings
9. portfolio
10. ourapproach
11. ourspecialisation
12. article
13. partnership
14. whatwedo
15. footer
16. waitloader
17. cursor
18. js
19. closebody
20. end
-->

<!-- Structure Content
1. nav
2. header
3. about
4. corporatevalues
5. greetings
6. portfolio
7. ourapproach
8. ourspecialisation
9. article
10. partnership
11. whatwedo
12. footer
-->
