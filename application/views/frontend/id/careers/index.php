<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>
<main class="main-root">
  <div id="dsn-scrollbar">
    <header>
      <div class="header-hero header-hero-2 ">
        <div class="container h-100">
          <div class="row align-items-center h-100">
            <div class="col-lg-12">
              <div class="contenet-hero">
                <h1>Jenjang Karir</h1>
                <h4>Mulai Bersama Kami</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="wrapper">
      <div class="root-contact">
        <!--<div class="container-fluid ">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31726.357398042626!2d106.8095850635772!3d-6.290695780331472!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1fa599ef523%3A0xce4d3f317f5ed3f4!2sMenara%20165%2C%20Jl.%20TB%20Simatupang%2C%20RT.3%2FRW.3%2C%20Cilandak%20Tim.%2C%20Kec.%20Ps.%20Minggu%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2012560!5e0!3m2!1sid!2sid!4v1605469786768!5m2!1sid!2sid" width="100%" height="480" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>-->

      <div class="container section-margin">
        <div class="row">
          <div class="col-lg-6">
            <div class="box-info-contact">
              <h3>Tertarik bergabung bersama kami ?</h3>
              <h5>Untuk Informasi Lebih Lanjut</h5>

              <ul>
                <li>
                  <span>Email</span>
                  <a href="#">INFO@BYGREENARCHITECTS.COM</a>
                </li>
                <li>
                  <span>Alamat</span>
                  <a href="#">MENARA 165, LANTAI 4<br>
                    JL. TB. SIMATUPANG KAV. 1<br>
                    CILANDAK TIMUR, PASAR MINGGU<br>
                    JAKARTA SELATAN (12560)</a>
                  </li>
                </ul>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-box">
                <h3>Kirim CV Kamu</h3>
                <?php echo form_open_multipart('in-id/form_contact/postEmail');?>
                <form id="contact-form" class="form" method="POST" action="">
                  <div class="messages"></div>
                  <div class="input__wrap controls">
                    <div class="form-group">

                      <div class="entry">
                        <label>Siapa Nama Kamu ?</label>
                        <input id="name" type="text" name="name" placeholder="Ketik Disini ..." required="required"
                        data-error="name is required.">
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <div class="entry">
                        <label>Apa Alamat Email Kamu ?</label>
                        <input id="email" type="email" name="email" placeholder="Type your Email Address"
                        required="required" data-error="Valid email is required.">
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <div class="entry">
                        <label>Lampiran CV</label>
                        <input type="file" name="resume" id="resume" accept=".doc,.docx, .pdf" required>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <div class="entry">
                        <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="image-zoom" data-dsn="parallax">
                      <button>Kirim CV</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>



      <?php
      include __DIR__ .('/../footer.php');
      include __DIR__ .('/../waitloader.php');
      include __DIR__ .('/../cursor.php');
      include __DIR__ .('/../js.php');
      include __DIR__ .('/../closebody.php');
      include __DIR__ .('/../end.php');
      ?>
