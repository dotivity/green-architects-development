
<footer class="footer section-top">
  <div class="container">
    <div class="footer-links p-relative">
      <div class="row">
        <div class="col-md-3 dsn-col-footer">
          <div class="footer-block">
            <div class="footer-logo">
              <a href="<?php echo site_url ('in-id')?>"><img src="<?php echo base_url ('assets_frontend/img/logo-dark.png')?>" alt=""></a>
            </div>

            <div class="footer-social">

              <ul>
                <li><a href="https://www.linkedin.com/company/bygreenarchitects" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="https://www.instagram.com/bygreenarchitects/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/bygreenarchitects" target="_blank"><i class="fab fa-facebook"></i></a></li>

              </ul>

            </div>
          </div>
        </div>

        <div class="col-md-3 dsn-col-footer">
          <div class="footer-block col-menu">
            <h4 class="footer-title">Navigasi</h4>
            <nav>
              <ul>
                <li><a href="<?php echo site_url ('in-id')?>">Beranda</a>
                </li>
                <li><a href="<?php echo site_url ('in-id/about')?>">Tentang Kami</a></li>
                <li><a href="<?php echo site_url ('in-id/greetings')?>">Sambutan</a></li>
                <li><a href="<?php echo site_url ('in-id/office')?>">Kantor Kami</a></li>
                <li><a href="<?php echo site_url ('in-id/whatwedo')?>">Apa Yang Kami Lakukan</a></li>
                <li><a href="<?php echo site_url ('in-id/works')?>">Pekerjaan</a></li>
                <li><a href="<?php echo site_url ('in-id/news')?>">Berita</a></li>
                <li><a href="<?php echo site_url ('in-id/careers')?>">Karir</a></li>
                <li><a href="<?php echo site_url ('in-id/contact')?>">Kontak</a></li>
              </ul>
            </nav>
          </div>
        </div>


        <div class="col-md-3 dsn-col-footer">
          <div class="col-address">
            <h4 class="footer-title">Alamat</h4>

            <p>Menara 165, Lantai 4<br>
              Jl. TB. Simatupang Kav. 1<br>
              Cilandak Timur, Pasar Minggu<br>
              Jakarta Selatan (12560)</p>
            </div>
          </div>


          <div class="col-md-3 dsn-col-footer">
            <div class="footer-block col-contact">
              <h4 class="footer-title">Kontak</h4>
              <p class="over-hidden"><strong>E</strong> <span>:</span><a class="link-hover"
                data-hover-text="info@bygreenarchitects.com" href="mailto:info@bygreenarchitects.com">info@bygreenarchitects.com</a>
              </p>
            </div>
          </div>
        </div>
      </div>


      <div class="copyright">
        <div class="text-center">
          <p>© 2021 By Green Architects</p>
        </div>
      </div>
    </div>
  </footer>
</div>
</main>
