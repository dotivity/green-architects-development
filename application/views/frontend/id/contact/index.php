<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>
<main class="main-root">
  <div id="dsn-scrollbar">
    <header>
      <div class="container header-hero section-margin">
        <div class="row">
          <div class="col-lg-6">
            <div class="contenet-hero">
              <h5>Hubungi</h5>
              <h1>Kontak Kami</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="wrapper">
      <div class="root-contact">
        <div class="container-fluid ">
            <iframe  style="filter: invert(90%)" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31726.357398042626!2d106.8095850635772!3d-6.290695780331472!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1fa599ef523%3A0xce4d3f317f5ed3f4!2sMenara%20165%2C%20Jl.%20TB%20Simatupang%2C%20RT.3%2FRW.3%2C%20Cilandak%20Tim.%2C%20Kec.%20Ps.%20Minggu%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2012560!5e0!3m2!1sid!2sid!4v1605469786768!5m2!1sid!2sid" width="100%" height="480" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            <!--Berbayar-->
            <!--<div class="map-custom" id="map" data-dsn-lat="-6.290354539788058" data-dsn-len="106.80960655532841" data-dsn-zoom="14">-->
        </div>

        <div class="container section-margin">
          <div class="row">
            <div class="col-lg-12">
              <div class="box-info-contact">
                <h3>Mulai Sebuah Proyek Baru ?</h3>
                <h5>Hubungi Kantor Kami</h5>
                <ul>
                  <li>
                    <span>Email</span>
                    <a href="#">INFO@BYGREENARCHITECTS.COM</a>
                  </li>
                  <li>
                    <span>Address</span>
                    <a href="#">MENARA 165, LANTAI 4<br>
                      JL. TB. SIMATUPANG KAV. 1<br>
                      CILANDAK TIMUR, PASAR MINGGU<br>
                      JAKARTA SELATAN (12560)</a>
                    </li>
                  </li>
                </ul>
              </div>
            </div>

            <!--<div class="col-lg-6">
              <div class="form-box">
                <h3>Write A Comment</h3>
                <form id="contact-form" class="form" method="post" action="http://theme.dsngrid.com/droow-l/contact.php" data-toggle="validator">
                  <div class="messages"></div>
                  <div class="input__wrap controls">
                    <div class="form-group">

                      <div class="entry">
                        <label>What's your name?</label>
                        <input id="form_name" type="text" name="name" placeholder="Type your name" required="required"
                          data-error="name is required.">
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <div class="entry">
                        <label>What's your email address?</label>
                        <input id="form_email" type="email" name="email" placeholder="Type your Email Address"
                          required="required" data-error="Valid email is required.">
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <div class="entry">
                        <label>What's up?</label>
                        <textarea id="form_message" class="form-control" name="message"
                          placeholder="Tell us about you and the world" required="required"
                          data-error="Please,leave us a message."></textarea>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>

                    <div class="image-zoom" data-dsn="parallax">
                      <button>Send Message</button>
                    </div>
                  </div>
                </form>
              </div>
            </div> -->
          </div>
        </div>
      </div>



          <?php
          include __DIR__ .('/../footer.php');
          include __DIR__ .('/../waitloader.php');
          include __DIR__ .('/../cursor.php');
          include __DIR__ .('/../js.php');
          include __DIR__ .('/../closebody.php');
          include __DIR__ .('/../end.php');
          ?>
