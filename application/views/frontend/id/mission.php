<section class="client-see-v2 section-margin">
    <div class="container">
        <div class="inner">
            <div class="left">
                <h2 class="title" data-dsn-grid="move-section" data-dsn-move="-60"
                    data-dsn-duration="100%" data-dsn-opacity="1" data-dsn-responsive="tablet">
                    <span class="text">Misi Kami</span>
                </h2>
            </div>

            <div class="items">
                <div class="bg" data-bottom="opacity: 0;margin-left:240px" data-50-center="opacity:1;margin-left:0px"></div>
                <div class="slick-slider">
                    <div class="item">
                        <div class="quote" data-bottom="opacity: 0" data-50-center="opacity:1">
                            <p><b>INDONESIA’S ARCHITECTURE OF THE 21ST CENTURY</b> <br> <br>Menciptakan bangunan yang inovatif dan setara di seluruh indonesia dan menyediakan konstruksi bangunan yang rendah karbon.</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="quote" data-bottom="opacity: 0" data-50-center="opacity:1">
                            <p><b>GREEN INTENSIVE</b> <br> <br>Berkontribusi untuk menciptakan lebih banyak akses ke ruang hijau, terutama di perkotaan, serta membantu setiap orang untuk mengefisienkan penggunaan energi.</p>
                        </div>

                    </div>

                    <div class="item">
                        <div class="quote" data-bottom="opacity: 0" data-50-center="opacity:1">
                            <p><b>A BUILDING THAT INSPIRES AND EDUCATES</b> <br> <br>Mendirikan bangunan yang ikonik untuk meningkatkan nilai pendidikan dan pariwisata serta dapat menarik perhatian dunia.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
