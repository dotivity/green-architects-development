<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>

<main class="main-root">
  <div id='my-scrollbar' data-scrollbar>

    <div class="box-seat">
      <div class="dsn-v-text">
        <div class="container-fluid">
          <div class="box-middle-text">
            <h2 data-dsn-animate="text">What We Do</h2>
            <p data-dsn-animate="text">We are shaping and supporting positive human behaviour through architecture design, interior, landscape, and urban design. We specialise in sustainable residential design to high rise design for all over Asia. Through technologies, we will take you to find your own definitions of what it means to be sustainable.</p>
          </div>
          <div class="inner-img" data-dsn-grid="move-up" data-overlay="9">
            <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/what_we_do.jpg')?>" alt="">
          </div>
        </div>
      </div>
    </div>

    <div class="wrapper">
      <div class="root-project">

        <div class="container intro-project section-margin">
          <div class="intro-text text-center">
            <div class="title-cover" data-dsn-grid="move-section" data-dsn-opacity="0.1"
            data-dsn-duration="170%" data-dsn-move="0%">
            WHAT WE DO
          </div>
          <div class="inner">
            <h2 data-dsn-animate="text">Our Product</h2>
            <p data-dsn-animate="up">
              We created our 3D wall panels locally and responsibly to minimise carbon footprint. Our designs resonates with nature and infused with philosophy.
            </p>
            <li>
              <ul>• Organic • Responsible • Affordable</ul>
            </li>
            <li>
              <ul>• Positive psychological impact • Durable • Ease of installation</ul>
            </li>
          </div>
        </div>
      </div>

      <div class="container-fluid gallery-col">
        <div class="row">
          <div class="col-md-4 box-im section-padding">
            <div class="image-zoom" data-dsn="parallax">
              <a class="single-image" href="<?php echo base_url ('assets_frontend/img/home/product/1.jpg')?>">
                <img src="<?php echo base_url ('assets_frontend/img/home/product/1.jpg')?>" alt="">
              </a>
            </div>
          </div>

          <div class="col-md-4 box-im section-padding">
            <div class="image-zoom" data-dsn="parallax">
              <a class="single-image" href="<?php echo base_url ('assets_frontend/img/home/product/2.jpg')?>">
                <img src="<?php echo base_url ('assets_frontend/img/home/product/2.jpg')?>" alt="">
              </a>
            </div>
          </div>

          <div class="col-md-4 box-im section-padding">
            <div class="image-zoom" data-dsn="parallax">
              <a class="single-image" href="<?php echo base_url ('assets_frontend/img/home/product/3.jpg')?>">
                <img src="<?php echo base_url ('assets_frontend/img/home/product/3.jpg')?>" alt="">
              </a>
            </div>
          </div>

        </div>
      </div>

      <div class="box-seat section-margin">
        <div class="container-fluid">
          <div class="inner-img" data-dsn-grid="move-up">
            <img src="<?php echo base_url ('assets_frontend/img/home/architecture_design_build.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
          </div>
          <div class="pro-text" data-dsn-grid="move-section">
            <h4 data-dsn-animate="text"><b>Architecture Design-Build</b></h4>
            <p data-dsn-animate="text">
              By trusting us as your contractor it allows a quicker start on site and better quality control to make your design into reality.
            </p>
            <p data-dsn-animate="text">
              As it is common to stumble into something unexpected, our involvement allows for a dynamic design process where we can re-detail around the design.
            </p>
          </div>
        </div>
      </div>

      <div class="box-seat-v1 section-margin">
        <div class="container-fluid">
          <div class="pro-text" style="z-index:10" data-dsn-grid="move-section">
            <h4 data-dsn-animate="text"><b>Interior Design-Build</b></h4>
            <p data-dsn-animate="text">
              We have a trusted in-house team and will deliver a quality end product with a one year guarantee.
            </p>
            <p data-dsn-animate="text">
              We offer competitive pricing to ensure our clients get the “most” with “less”.
            </p>
          </div>
          <div class="inner-img-v1" data-dsn-grid="move-up">
            <img src="<?php echo base_url ('assets_frontend/img/home/interior_design_build.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
          </div>
        </div>
      </div>

    </div>
    </div>




    <?php
    include __DIR__ .('/../footer.php');
    include __DIR__ .('/../waitloader.php');
    include __DIR__ .('/../cursor.php');
    include __DIR__ .('/../js.php');
    include __DIR__ .('/../closebody.php');
    include __DIR__ .('/../end.php');
    ?>
