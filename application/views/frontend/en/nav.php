<!-- Nav Bar -->
<div class="dsn-nav-bar">
  <div class="site-header">
    <div class="extend-container">
      <div class="inner-header">
        <div class="main-logo">
          <a href="<?php echo site_url ('en-us')?>">
            <img class="dark-logo" src="<?php echo base_url ('assets_frontend/img/logo-dark.png')?>" alt="" />
            <img class="light-logo" src="<?php echo base_url ('assets_frontend/img/logo.png')?>" alt="" />
          </a>
        </div>
      </div>
      <nav class=" accent-menu main-navigation">
        <ul class="extend-container">
          <li><a href="<?php echo site_url ('en-us')?>">Home</a></li>
          <li><a href="<?php echo site_url ('en-us/about')?>">About</a></li>
          <li><a href="<?php echo site_url ('en-us/greetings')?>">Greetings</a></li>
          <li><a href="<?php echo site_url ('en-us/office')?>">Our Office</a></li>
          <li><a href="<?php echo site_url ('en-us/whatwedo')?>">What We Do</a></li>
          <li><a href="<?php echo site_url ('en-us/works')?>">Works</a></li>
          <li><a href="<?php echo site_url ('en-us/news')?>">News</a></li>
          <li><a href="<?php echo site_url ('en-us/careers')?>">Careers</a></li>
          <li><a href="<?php echo site_url ('en-us/contact')?>">Contact</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="header-top header-top-hamburger">
    <div class="header-container">
      <div class="logo main-logo">
        <a href="<?php echo site_url ('en-us')?>">
          <img class="dark-logo" src="<?php echo base_url ('assets_frontend/img/logo-dark.png')?>" alt="" />
          <img class="light-logo" src="<?php echo base_url ('assets_frontend/img/logo.png')?>" alt="" />
        </a>
      </div>

      <div class="menu-icon" data-dsn="parallax" data-dsn-move="5" id="menu-icon">
        <div class="icon-m">
          <i class="menu-icon-close fas fa-times"></i>
          <span class="menu-icon__line menu-icon__line-left"></span>
          <span class="menu-icon__line"></span>
          <span class="menu-icon__line menu-icon__line-right"></span>
        </div>

        <div class="text-menu">
          <div class="text-button">Menu</div>
          <div class="text-open">Open</div>
          <div class="text-close">Close</div>
        </div>
      </div>


      <div class="menu-icon" style="z-index:2">
        <a class="text-menu" href="<?php echo site_url ('en-us')?>"  style="right:20px">
          <div class="text-menu">
            <img style="width:3%" src="https://upload.wikimedia.org/wikipedia/commons/a/ae/Flag_of_the_United_Kingdom.svg"/>
          </div>
        </a>
      </div>

      <div class="menu-icon"  style="z-index:1">
        <a class="text-menu" href="<?php echo site_url ('in-id')?>">
          <div class="text-menu">
            <img style="width:3%" src="https://upload.wikimedia.org/wikipedia/commons/9/9f/Flag_of_Indonesia.svg"/>
          </div>
        </a>
      </div>

      <div class="nav">
        <div class="inner">
          <div class="nav__content">

          </div>
        </div>
      </div>
      <div class="nav-content">
        <div class="inner-content">
          <address class="v-middle">
            <span>Menara 165, 4th Floor,</span>
            <span>TB. Simatupang Street Kav. 1,</span>
            <span>Cilandak Timur, Pasar Minggu,</span>
            <span>Jakarta Selatan (12560)</span>
          </address>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- End Nav Bar -->
