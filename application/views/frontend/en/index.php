<?php
include "html.php";
include "openbody.php";
include "preloader.php";
include "nav.php";
include "header.php";
include "about.php";
include "vision.php";
include "mission.php";
include "corporatevalues.php";
include "greetings.php";
include "portfolio.php";
include "video.php";
include "importance.php";
include "ourapproach.php";
include "ourspecialisation.php";
include "partnership.php";
include "whatwedo.php";
include "maps.php";
include "hiring.php";
include "footer.php";
include "waitloader.php";
include "cursor.php";
include "js.php";
include "closebody.php";
include "end.php";
 ?>
