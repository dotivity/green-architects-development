<div class="box-seat box-seat-full section-margin">
  <h2 class="title-main" style="margin-bottom:50px;text-align:center">Greetings From President Commissioner</h2>
  <div class="container-fluid">
    <div class="inner-img" data-dsn-grid="move-up">
      <img style="object-position: 25% 100%;" data-dsn-scale="1" data-dsn-y="30%" src="<?php echo base_url ('assets_frontend/img/greetings/ari.jpg')?>" alt="">
    </div>
    <div class="pro-text" style="background-color: rgb(0 0 0 / 0%);">
      <h3 style="font-size:1.8em;color:#000000" data-bottom="opacity: 0;margin-bottom:240px" data-200-center="opacity:1;margin-bottom:0px"> Dr. (H.C) Ary Ginanjar Agustian</h3>
      <h3 style="font-size:1em;color:#000000" data-bottom="opacity: 0;" data-200-center="opacity:1;">President Commissioner | Founder ESQ Group</h3>
    </div>
  </div>
</div>


<div class="wrapper">
  <div class="root-project">
    <div class="container intro-project section-margin">
      <div class="intro-text">
        <div class="inner">
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            “Nowadays we talk a lot about renewable energy, but we forget about the forgotten energy. People are busy with how to build new energy, but<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            people forget how we save and organise energy from a different side, namely from the architect side. The costs incurred due to buildings that do<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            not have the basis for sustainable architecture are not counted, thus the losses that are generated due to ineffectiveness and inefficiency are<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            countless. We can imagine if one building could save around Rp. 100,000,000 in a month, and multiplied by a thousand or even ten thousand<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            buildings, how much money can you get from the calculation results? Where the money can of course be used to build humanity, when the<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            Indonesian nation needs so much help in an atmosphere full of economic challenges”.
          </p>
          <br><br>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            “Green Architects not only creates a sustainable ecosystem, but how we can improve the quality of life. The presence of Green Architects is <br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            important in Indonesia in order to become a Golden Indonesia 2045, as a global force country it will be difficult to achieve if we are wasteful<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            of energy, our buildings still do not use green principles, far from green buildings which result in wasted energy that could have been used for other<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            needs, to build a civilization in Indonesia. Congratulations on the presence of Green Architects, I hope that your contribution will help accelerate<br>
          </p>
          <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
            towards the Golden Indonesia 2045. Regards, Green Architects!”.<br>
          </p>

          <p class="greetings-mobile" style="margin-top: -50px;">
            “Nowadays we talk a lot about renewable energy, but we forget about the forgotten energy. People are busy with how to build new energy, but people forget how we save and organise energy from a different side, namely from the architect side. The costs incurred due to buildings that do not have the basis for sustainable architecture are not counted, thus the losses that are generated due to ineffectiveness and inefficiency are countless. We can imagine if one building could save around Rp. 100,000,000 in a month, and multiplied by a thousand or even ten thousand buildings, how much money can you get from the calculation results? Where the money can of course be used to build humanity, when the Indonesian nation needs so much help in an atmosphere full of economic challenges”.<br><br>

            “Green Architects not only creates a sustainable ecosystem, but how we can improve the quality of life. The presence of Green Architects is important in Indonesia in order to become a Golden Indonesia 2045, as a global force country it will be difficult to achieve if we are wasteful of energy, our buildings still do not use green principles, far from green buildings which result in wasted energy that could have been used for other needs, to build a civilization in Indonesia. Congratulations on the presence of Green Architects, I hope that your contribution will help accelerate towards the Golden Indonesia 2045. Regards, Green Architects!”.
          </p>

        </div>
      </div>
    </div>
  </div>
</div>


<div class="wrapper">
  <div class="root-about">

    <div class="box-seat box-seat-full">
      <div class="container-fluid">
        <div class="inner-img d-none d-sm-block" data-dsn-grid="move-up">
          <img class="reza-photos" src="<?php echo base_url ('assets_frontend/img/greetings/rima.jpg')?>" alt="" data-dsn-y="35%" data-dsn-scale="1">
        </div>
        <div class="inner-img d-block d-sm-none">
          <img class="reza-photos" style="object-fit: cover; object-position: 10% 50%;" src="<?php echo base_url ('assets_frontend/img/greetings/rima.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
        </div>
        <div class="pro-text">
          <h3 style="padding-bottom: 40px;" data-bottom-center="opacity: 0;" data-bottom="opacity:1;">GREETINGS FROM BOARD OF DIRECTORS</h3>
          <h3 style="font-size:1.6em" data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Rima Khansa Nurani, B.A.Sc., M.Sc.</h3>
          <h3 style="font-size:1em" data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Managing Director | University of Liverpool graduate</h3>
          <p style="font-size:13px" data-dsn-animate="up" data-bottom-center="opacity: 0;"  data-200-center="opacity:1;">
            “After graduating from Australia, I see there is a need to focus on sustainable design, I have seen how architecture can disconnect or connect people. I believe we are the carer of the world, and we should dream and aim higher. So I took an action and pursue my study in Sustainable Environmental Design in Architecture (SEDA) in the UK to explore the idea.<br><br>

            While doing my research and visiting many countries, I found that there is a huge gap between the buildings across Indonesia and the world. Therefore, through Green Architects, we aim to popularise green design in Indonesia, creating buildings that educate, and enhancing people’s productivity and wellbeing”.
          </p>
        </div>
      </div>
    </div>

    <div class="box-seat box-seat-full">
      <div class="container-fluid">
        <div class="inner-img d-none d-sm-block" data-dsn-grid="move-up">
          <img class="reza-photos" src="<?php echo base_url ('assets_frontend/img/greetings/reza.jpg')?>" alt="" data-dsn-y="35%" data-dsn-scale="1">
        </div>
        <div class="inner-img d-block d-sm-none">
          <img class="reza-photos" style="object-fit: cover; object-position: 80% 50%;" src="<?php echo base_url ('assets_frontend/img/greetings/reza.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
        </div>
        <div class="pro-text1">
          <h3 style="padding-bottom: 40px;" data-bottom-center="opacity: 0;" data-bottom="opacity:1;">GREETINGS FROM BOARD OF DIRECTORS</h3>
          <h3 style="font-size:1.6em" data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Ahmad Reza Hariyadi, S.M., M.Sc.</h3>
          <h3 style="font-size:1em"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Executive Director | Liverpool John Moores University graduate</h3>
          <p style="font-size:13px" data-dsn-animate="up"  data-bottom-center="opacity: 0;"  data-200-center="opacity:1;">“Not only focusing on the environmentally friendly design that we created, but Green Architects also focus on our shareholders and stakeholder's well-being. Hence, we have a sustainable business process and giving a positive contribution to the environment, society, and our country. Moreover, to give the best services for our clients, Green Architects keep pace with time to use the new concepts and implement the newest technology in every design that we created”.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
