<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend/images/favicon.ico')?>">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/vendor/bootstrap/css/bootstrap.min.css')?>">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/vendor/animate/animate.css')?>">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/vendor/css-hamburgers/hamburgers.min.css')?>">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/vendor/select2/select2.min.css')?>">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/css/util.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_login/css/main.css')?>">
  <!--===============================================================================================-->
</head>
<body>

  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic js-tilt" data-tilt>
          <img src="<?php echo base_url ('assets_login/images/img-01.png')?>" alt="IMG">
        </div>
        <form class="login100-form validate-form"  action="<?php echo site_url('gotobackend/login'); ?>" method="post">
          <span class="login100-form-title">
            Green Architects Login
          </span>
          <?php if (isset($error_login)): ?>
            <div class="alert alert-danger" role="alert">
              <?php echo $error_login['login']; ?>
            </div>
          <?php endif; ?>
          <div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="row[username]" placeholder="Username">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user" aria-hidden="true"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <input class="input100" type="password" name="row[password]" placeholder="Password">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
          </div>

            <div class="wrap-input100 validate-input">
              <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div>
            </div>

          <div class="container-login100-form-btn">
            <input class="login100-form-btn" type="submit" value="Login"  name="btn-login">
          </div>

          <div class="text-center p-t-12">
          </div>

          <div class="text-center p-t-136">
          </div>
        </form>
      </div>
    </div>
  </div>




  <!--===============================================================================================-->
  <script src="<?php echo base_url ('assets_login/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
  <!--===============================================================================================-->
  <script src="<?php echo base_url ('assets_login/vendor/bootstrap/js/popper.js')?>"></script>
  <script src="<?php echo base_url ('assets_login/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
  <!--===============================================================================================-->
  <script src="<?php echo base_url ('assets_login/vendor/select2/select2.min.js')?>"></script>
  <!--===============================================================================================-->
  <script src="<?php echo base_url ('assets_login/vendor/tilt/tilt.jquery.min.js')?>"></script>
  <script >
  $('.js-tilt').tilt({
    scale: 1.1
  })
  </script>
  <!--===============================================================================================-->
  <script src="<?php echo base_url ('assets_login/js/main.js')?>"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>

</body>
</html>
