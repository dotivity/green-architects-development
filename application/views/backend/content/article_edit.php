

<?php
include 'html.php';
include 'css.php'; ?>

</head>

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

  <!-- BEGIN: Header-->
  <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-container content">
        <div class="navbar-collapse" id="navbar-mobile">
          <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav">
              <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
            </ul>
            <ul class="nav navbar-nav bookmark-icons">
              <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
              <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
              <!--     i.ficon.feather.icon-menu-->
              <li class="nav-item d-none d-lg-block"><h3 class="content-header-title float-left mb-0">Edit Article</h3></li>
            </ul>
            <ul class="nav navbar-nav">
            </ul>
          </div>
          <ul class="nav navbar-nav float-right">
            <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>

            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
              <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600"><?php echo $this->session->userdata('cibb_username'); ?></span><span class="user-status">Available</span></div><span><img class="round" src="<?php echo base_url ('assets_backend/app-assets/images/portrait/small/avatar.png')?>" alt="avatar" height="40" width="40"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="<?=site_url()?>gotobackend/logout"><i class="feather icon-power"></i> Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>


<ul class="main-search-list-defaultlist-other-list d-none">
  <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100 py-50">
    <div class="d-flex justify-content-start"><span class="mr-75 feather icon-alert-circle"></span><span>No results found.</span></div>
  </a></li>
</ul>


<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">
        <h2 class="brand-text mb-0">Green Architects</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 success toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon success" data-ticon="icon-disc"></i></a></li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url('backend/dashboard')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

      <?php if ($this->session->userdata('admin_area') != 0): ?>
        <li class=" navigation-header"><span>ACCOUNT</span>
        </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
          </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li><a href="<?php echo site_url('backend/portfolio')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>
          <li class="active"><a href="<?php echo site_url('backend/article')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Article</span></a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<!--content -->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
          <div class="col-12">
          </div>
        </div>
      </div>
      <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
      </div>
    </div>
    <div class="content-body">


      <!-- Basic Vertical form layout section start -->

      <section id="basic-vertical-layouts">
        <div class="row match-height">
          <div class="col-md-12 col-12">
            <div class="card">

              <div class="card-header">

              </div>
              <div class="card-content">
                <div class="card-body">
                  <?php echo validation_errors(); ?>
                  <?php if(isset($error)){print $error;}?>
                  <!-- general form elements -->
                  <?php echo form_open_multipart('backend/article/updatedata');?>
                  <form class="form form-vertical" action="" method="post">
                    <div class="form-body">
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <label for="pic_title">Title English</label>
                            <input type="text" id="pic_title" class="form-control" name="pic_title" value="<?=$data->pic_title?>">
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <label for="pic_title">Title Bahasa Indonesia</label>
                            <input type="text" id="pic_title1" class="form-control" name="pic_title1" value="<?=$data->pic_title1?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-6" >
                          <label for="label-textarea">Project Data English</label>
                          <fieldset class="form-label-group">
                            <textarea name="pic_project" class="form-control" id="editor2" placeholder="Description English"><?=$data->pic_project?></textarea>
                          </fieldset>
                        </div>
                        <div class="col-6" >
                          <label for="label-textarea">Project Data Indonesia</label>
                          <fieldset class="form-label-group">
                            <textarea name="pic_project1" class="form-control" id="editor3" placeholder="Description English"><?=$data->pic_project1?></textarea>
                          </fieldset>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-6" >
                          <label for="label-textarea">Description English</label>
                          <fieldset class="form-label-group">
                            <textarea name="pic_desc" class="form-control" id="editor" placeholder="Description English"><?=$data->pic_desc?></textarea>
                          </fieldset>
                        </div>
                        <div class="col-6" >
                          <label for="label-textarea">Description Bahasa Indonesia</label>
                          <fieldset class="form-label-group">
                            <textarea name="pic_desc1" class="form-control" id="editor1" placeholder="Description Bahasa Indonesia" ><?=$data->pic_desc1?></textarea>
                          </fieldset>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12">
                          <label for="label-textarea">Image</label>
                          <div class="col-sm-12 data-field-col">
                            <input type="file" class="dropzone dropzone-area" name="userfile[]" required>
                            <div class="dz-message">
                              Re-Upload Image
                            </div>
                          </input>
                        </div>
                      </div>
                    </div>
                    <!-- ID -->
                    <input type="hidden" name="pic_id" value="<?=$data->pic_id?>">
                    <input type="hidden" name="filelama" id="filelama" value="<?=$data->pic_file?>">

                    <div class="col-12">
                      <button type="submit" name="fileSubmit" class="btn btn-success mr-1 mb-1">Submit</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

</div>
</div>
</div>

<?php
include 'footer.php';
include 'js.php';
include 'end.php';?>
