<!-- Dashboard Analytics Start -->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
          <div class="col-12">
            <h2 class="content-header-title float-left mb-0">Site Map</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="content-body">

      <!-- Anchors and buttons start -->
      <section id="anchors-n-buttons">
        <div class="row match-height">


          <div class="col-lg-4 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Front End | English</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="list-group">
                    <a href="<?php echo site_url ()?>" target="_blank" class="list-group-item active">Home</a>
                    <a href="<?php echo site_url ('en-us/publisher')?>" target="_blank" class="list-group-item list-group-item-action">Publisher</a>
                    <a href="<?php echo site_url ('en-us/promo')?>" target="_blank" class="list-group-item list-group-item-action">Promo</a>
                    <a href="<?php echo site_url ('en-us/storex')?>" target="_blank" class="list-group-item list-group-item-action">Store-X</a>
                    <a href="<?php echo site_url ('en-us/offdex')?>" target="_blank" class="list-group-item list-group-item-action">Offde-X</a>
                    <a href="<?php echo site_url ('en-us/optimax')?>" target="_blank" class="list-group-item list-group-item-action">Optima-X</a>
                    <a href="<?php echo site_url ('en-us/datax')?>" target="_blank" class="list-group-item list-group-item-action">Data-X</a>
                    <a href="<?php echo site_url ('en-us/digitax')?>" target="_blank" class="list-group-item list-group-item-action">Digita-X</a>
                    <a href="<?php echo site_url ('error')?>" target="_blank" class="list-group-item list-group-item-action">404 Page</a>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Front End | Bahasa Indonesia</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="list-group">
                    <a href="<?php echo site_url ('in-id/homepage')?>" target="_blank" class="list-group-item active">Home</a>
                    <a href="<?php echo site_url ('in-id/publisher')?>" target="_blank" class="list-group-item list-group-item-action">Publisher</a>
                    <a href="<?php echo site_url ('in-id/promo')?>" target="_blank" class="list-group-item list-group-item-action">Promo</a>
                    <a href="<?php echo site_url ('in-id/storex')?>" target="_blank" class="list-group-item list-group-item-action">Store-X</a>
                    <a href="<?php echo site_url ('in-id/offdex')?>" target="_blank" class="list-group-item list-group-item-action">Offde-X</a>
                    <a href="<?php echo site_url ('in-id/optimax')?>" target="_blank" class="list-group-item list-group-item-action">Optima-X</a>
                    <a href="<?php echo site_url ('in-id/datax')?>" target="_blank" class="list-group-item list-group-item-action">Data-X</a>
                    <a href="<?php echo site_url ('in-id/digitax')?>" target="_blank" class="list-group-item list-group-item-action">Digita-X</a>
                    <a href="<?php echo site_url ('in-id/error')?>" target="_blank" class="list-group-item list-group-item-action">404 Page</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Back End</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="list-group">
                    <a href="<?=site_url('backend/dashboard')?>" class="list-group-item active">Home</a>
                    <a href="<?php echo site_url('backend/client')?>" target="_blank" class="list-group-item list-group-item-action">Logo Client</a>
                    <a href="<?php echo site_url('backend/publisher')?>" target="_blank" class="list-group-item list-group-item-action">Logo Publisher</a>
                    <a href="<?php echo site_url('backend/promo')?>" target="_blank" class="list-group-item list-group-item-action">Promo</a>
                    <a href="<?php echo site_url('backend/campaign')?>" target="_blank" class="list-group-item list-group-item-action">Campaign</a>
                    <a href="<?php echo site_url('backend/testimonial')?>" target="_blank" class="list-group-item list-group-item-action">Testimonial</a>
                    <a href="<?php echo site_url('backend/knowledgebase')?>" target="_blank" class="list-group-item list-group-item-action">Knowledge Base</a>
                    <?php if ($this->session->userdata('admin_area') != 0): ?>
                      <a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">My Account</a>
                      <a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">List Account</a>
                      <a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">Add Account</a>
                      <a href="<?php echo site_url('backend/user/edit_add/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">Edit Account</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Anchors and buttons end -->

      <!-- Custom Listgroups start -->

      <!-- Custom Listgroups end -->


      <!-- Flush and Horizontal list group starts -->

      <!-- Flush and Horizontal list group Ends -->


      <!--List group with tabs Starts-->

      <!--List group with tabs Ends-->

    </div>
  </div>
</div>
<!-- Dashboard Analytics end -->

<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
