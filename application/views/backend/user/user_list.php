

<?php
include 'html.php';
include 'css.php'; ?>

</head>

<?php include 'header.php'; ?>

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">

        <h2 class="brand-text mb-0">Green Architects</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 success toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon success" data-ticon="icon-disc"></i></a></li>

    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url()?>backend/dashboard"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

            <?php if ($this->session->userdata('admin_area') != 0): ?>
      <li class=" navigation-header"><span>ACCOUNT</span>
      </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li class="active"><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
           </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li><a href="<?php echo site_url('backend/client')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Client</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/publisher')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Publisher</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/promo')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Promo</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/campaign')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/testimonial')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Testimonial</span></a>
          </li>

        </ul>
      </li>

      <li class=" navigation-header"><span>GUIDELINES</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-info"></i><span class="menu-title" data-i18n="User">Knowledge Base</span></a>
        <ul class="menu-content">
          <li>
            <a href="<?php echo site_url('backend/knowledgebase')?>">
              <i class="feather icon-circle"></i>
              <span class="menu-item" data-i18n="View">
                Documentations
              </span>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</div>


<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
          <div class="col-12">
          </div>
        </div>
      </div>
      <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
        <div class="form-group breadcrum-right">
        </div>
      </div>
    </div>
    <div class="row" id="table-hover-row">
      <div class="col-12">
        <div class="card">

          <div class="card-content">
            <div class="card-body">
              <?php if (isset($error)): ?>
                <div class="alert alert-error">
                  <a class="close" data-dismiss="alert" href="#">&times;</a>
                  <h4 class="alert-heading">Error!</h4>
                  <?php if (isset($error['password'])): ?>
                    <div>- <?php echo $error['password']; ?></div>
                  <?php endif; ?>
                </div>
              <?php endif; ?>
              <?php if (isset($tmp_success)): ?>
                <div class="alert alert-success">
                  <a class="close" data-dismiss="alert" href="#">&times;</a>
                  <h4 class="alert-heading">Data Saved</h4>
                </div>
              <?php endif; ?>
            </div>
            <div class="table-responsive">
              <table class="table table-hover mb-0">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th>Company</th>
                    <th>Mobile</th>
                    <th>Website</th>
                    <th>Facebook</th>
                    <th>Instagram</th>
                    <th>Twitter</th>
                    <th>LinkedIn</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($users as $user): ?>
                    <tr>
                      <td><?php echo $user->username; ?></td>
                      <td><?php echo $user->name; ?></td>
                      <td><?php echo $user->gender; ?></td>
                      <td><?php echo $user->email; ?></td>
                      <td><?php echo $user->roles; ?></td>
                      <td><?php echo $user->company; ?></td>
                      <td><?php echo $user->mobile; ?></td>
                      <td><?php echo $user->website; ?></td>
                      <td><?php echo $user->facebook; ?></td>
                      <td><?php echo $user->instagram; ?></td>
                      <td><?php echo $user->twitter; ?></td>
                      <td><?php echo $user->linkedin; ?></td>
                      <td><a title="edit" href="<?php echo site_url('backend/user/user_edit').'/'.$user->id; ?>"><span class="feather icon-edit"></span></a>
                        <a title="delete"  id="user_id_<?php echo $user->id; ?>" href="<?php echo site_url('backend/user/user_delete').'/'.$user->id; ?>"><span class="feather icon-delete"></span></a></td>
                      </tr>
                    <?php endforeach; ?>
                    <div id="embed-api-auth-container"></div>
                    <div id="chart-1-container"></div>
                    <div id="chart-2-container"></div>
                    <div id="view-selector-1-container"></div>
                    <div id="view-selector-2-container"></div>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- END: Content-->

  <?php
  include 'footer.php';
  include 'js.php';
  include 'end.php';?>
