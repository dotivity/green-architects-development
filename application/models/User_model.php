<?php

class User_model extends CI_Model {
  public $error       = array();
  public $error_count = 0;

  public function __construct()
  {
    parent::__construct();
  }

  public function check_role()
  {
    $user_id = $this->session->userdata('cibb_user_id');
    // get roles
    if ($user_id) {
      $row = $this->db->get_where('cibb_users', array('id' => $user_id))->row();
      $roles = $this->db->get_where('cibb_roles', array('id' => $row->role_id))->row_array();
      foreach ($roles as $key => $value) {
        $this->session->set_userdata($key, $value);
      }
    }
  }

  public function check_login()
  {
    $row = $this->input->post('row');
    $key = $this->config->item('encryption_key');
    $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));

    $data = array('username' => $row['username']);

    $query = $this->db->get_where('cibb_users', $data);

    $plain_password = '';

    if ( ($query->num_rows() == 1) ) {
      $user = $query->row();
      $plain_password = $this->encrypt->decode($user->password, $key);
    }

    $userIp=$this->input->ip_address();

    $secret= $this->config->item('google_secret');

    $credential = array(
      'secret' => $secret,
      'response' => $this->input->post('g-recaptcha-response')
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
      curl_setopt($verify, CURLOPT_POST, true);
      curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
      curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($verify);

      $status= json_decode($response, true);

    // if user found
    if ( ($query->num_rows() == 1) && ($plain_password == $row['password'])) {
      $row = $query->row();
      $this->session->set_userdata('cibb_logged_in', 1);
      $this->session->set_userdata('cibb_user_id'  , $row->id);
      $this->session->set_userdata('cibb_username' , $row->username);
      $this->session->set_userdata('cibb_user_roleid' , $row->role_id);

      // get roles
      $roles = $this->db->get_where('cibb_roles', array('id' => $row->role_id))->row_array();
      foreach ($roles as $key => $value) {
        $this->session->set_userdata($key, $value);
      }


      } else {
        $this->error['login'] = 'User not found or wrong password';
        $this->error_count = 1;
      }
    }

    public function register()
    {
      $row = $this->input->post('row');


      // check username
      $is_exist_username = $this->db->get_where('cibb_users',
      array('username' => $row['username']))->num_rows();
      if ($is_exist_username > 0) {
        $this->error['username'] = 'Username already in use';
      }
      if (strlen($row['username']) < 5) {
        $this->error['username'] = 'Username minimum 5 character';
      }

      // check password
      if ($row['password'] != $this->input->post('password2')) {
        $this->error['password'] = 'Password not match';
      } else if (strlen($row['password']) < 5) {
        $this->error['password'] = 'Password minimum 5 character';
      }

      if (count($this->error) == 0) {
        $key = $this->config->item('encryption_key');
        $row['password'] = $this->encrypt->encode($row['password'], $key);
        $this->db->insert('cibb_users', $row);
      } else {
        $this->error_count = count($this->error);
      }

      $data['nama']   =    $this->input->post('name');
      $data['roles']   =    $this->input->post('roles');
      $data['email']   =    $this->input->post('email');
      $data['company']   =    $this->input->post('company');
      $data['mobile']   =    $this->input->post('mobile');
      $data['website']   =    $this->input->post('website');
      $data['gender']   =    $this->input->post('gender');
      $data['facebook']   =    $this->input->post('facebook');
      $data['instagram']   =    $this->input->post('instagram');
      $data['twitter']   =    $this->input->post('twitter');
      $data['linkedin']   =    $this->input->post('linkedin');

      $data['role_id']   =    $this->input->post('role_id');
    }
  }
